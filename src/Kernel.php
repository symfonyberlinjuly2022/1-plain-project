<?php

namespace App;

use App\Controller\ApiAwareInterface;
use App\Listener\EntityListenerInterface;
use App\Listener\OnFlushLifecycleListenerInterface;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    protected function build(ContainerBuilder $container)
    {
        $container
            ->registerForAutoconfiguration(EntityListenerInterface::class)
            ->addTag('doctrine.orm.entity_listener', ['lazy' => true]);

        $container
            ->registerForAutoconfiguration(OnFlushLifecycleListenerInterface::class)
            ->addTag('doctrine.event_listener', ['event' => 'onFlush']);

        $container
            ->registerForAutoconfiguration(ApiAwareInterface::class)
            ->addMethodCall('setApi', [new Reference('App\API\Client')]);
    }
}
